import { ADDPAGE_SHOW, ADDPAGE_HIDE, ADDPAGE_ADD_FIELD, ADDPAGE_REMOVE_FIELD, ADDPAGE_EDIT_FIELD, FIELD_TYPE } from "../actions";

const defaultState = {
    active: false,
    data: {
        icon: "fa fa-link",
        title: "Epta",
        fields: [
            {
                id: 0,
                type: FIELD_TYPE.TEXT,
                text: "Epta"
            },
            {
                id: 1,
                type: FIELD_TYPE.LINK,
                text: "link"
            },
        ]
    }
}

const addpage = (state = defaultState, action) => {
    switch (action.type) {
        case ADDPAGE_SHOW:
            return Object.assign({}, state, {active: true})
        case ADDPAGE_HIDE:
            return Object.assign({}, state, {active: false})
        case ADDPAGE_ADD_FIELD:
            return Object.assign({}, state, {data: Object.assign({}, state.data, {fields: [...state.data.fields, action.field]})})
        case ADDPAGE_REMOVE_FIELD:
            return Object.assign({}, state, {data: Object.assign({}, state.data, {fields: state.data.fields.filter((field) => field.id !== action.id)})})
        case ADDPAGE_EDIT_FIELD:
            console.log("Eblanceg");
            console.log(state);
            return Object.assign({}, state, {
                data: Object.assign({}, state.data, {
                    fields: state.data.fields.map((field) => {
                        if (field.id === action.field.id) {
                            return Object.assign({}, field, {text: action.field.value});
                        } else {
                            return field;
                        }
                    })
                })
            });
            // console.log(getStuffById(0));
            // return state;
        default:
            return state
    }
}

export default addpage


export const getStuffById = (stuffId) => {
    let resp = {
        active: false,
        data: {
            icon: "fa fa-link",
            title: "Epta",
            fields: [
                {
                    type: "link",
                    text: "Epta"
                },
                {
                    type: "text",
                    text: "link"
                },
            ]
        }
    }

    let id = 0;

    return resp.data.fields.map(field => ({...field, id: id++}))
}