import { ALERT } from '../actions';

const defaultState = {
    active: false
}

const alert = (state = defaultState, action) => {
    switch(action.type) {
        case ALERT.SHOW:
            return {
                text: action.text,
                active: true
            }
        case ALERT.HIDE:
            return {
                active: false
            }
        default:
            return state
    }
}

export default alert