import { COVER_SHOW, COVER_HIDE } from '../actions'

const defaultState = {
    active: false
}

const cover = (state = defaultState, action) => {
    switch (action.type) {
        case COVER_SHOW:
            return {active: true}
        case COVER_HIDE:
            return {active: false}
        default:
            return state
    }
}

export default cover