import { combineReducers } from 'redux'
import alert from './alert'
import menu from './menu'
import AddPageReducer from './AddPageReducer'
import navbar from './navbar'
// import cover from './cover'
import addpage from './addpage'

export default combineReducers({
    alert,
    menu,
    AddPageReducer,
    navbar,
    addpage,
    // cover
});