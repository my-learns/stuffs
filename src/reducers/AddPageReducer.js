const defaultState = {
    active: false,
}

const AddPageReducer = (state = defaultState, action) => {
    switch(action.type) {
        case "DEF":
            return {}
        default:
            return state
    }
}

export default AddPageReducer