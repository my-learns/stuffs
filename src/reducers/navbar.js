import { DRP_SHOW, DRP_HIDE, DRP_SHOW_HIDE, DRP_LEFT_SHOW_HIDE } from '../actions'

const defaultState = {
    active: false,
    dropleftActive: false
};

const navbar = (state = defaultState, action) => {
    switch (action.type) {
        case DRP_LEFT_SHOW_HIDE:
            // const st = state;
            
            // console.log("We are here");
            // console.log(state);
            // st.dropleftActive = !st.dropleftActive;
            return {
                active: false,
                dropleftActive: !state.dropleftActive
            };
        case DRP_SHOW_HIDE:
            return {active: !state.active}
        case DRP_SHOW:
            return {active: true}
        case DRP_HIDE:
            return {active: false}
        default:
            return state
    }
}

export default navbar