import { MENU } from '../actions'

const defaultState = {
    active: false,
}

const menu = (state = defaultState, action) => {
    switch(action.type) {
        case MENU.SHOW:
            return {active: true}
        case MENU.HIDE:
            return {active: false}
        default:
            return state
    }
}

export default menu;