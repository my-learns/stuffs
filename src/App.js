import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Main from './main/Main';
import "./_base.scss";
import Alert from './containers/Alert';
// import Button from './containers/Button';
import Navbar from './containers/Navbar';
import Menu from './containers/Menu';
// import { showAlert } from './actions';
import AddPage from './containers/_addpage'

export default function App() {
	return (
		<Router>
			<Navbar />
			<Menu />
			<Switch>
				<Route path="/">
					<Main/>
				</Route>
			</Switch>
			<AddPage />
			<Alert active={false} />
		</Router>
	);
}