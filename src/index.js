import React from 'react'
import { render } from 'react-dom'
import 'bootstrap/dist/css/bootstrap.min.css'
// import App from './App'
import App from './Main'

import { Provider } from 'react-redux'
import { createStore } from 'redux'
import rootReducer from './reducers'

const store = createStore(rootReducer)

render(
	<Provider store={store}>
    	<App />
	</Provider>,
  document.getElementById('root')
);