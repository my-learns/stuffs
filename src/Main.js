import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import "./_base.scss";
import Alert from './containers/Alert';
import ButtonCircle from './containers/ButtonCircle';
import Navbar from './containers/Navbar'
import Menu from './containers/Menu'
import SettingsPage from './main/pages/SettingsPage'
import ProfilePage from './main/pages/ProfilePage'
import StuffPage from './main/pages/StuffPage'
import AddPage from './containers/AddPage'

import Addpage from './containers/_addpage'

export default function App() {
	return (
		<Router>
			<Navbar>
				<ButtonCircle ligth={true} onClick={() => console.log("Epta")}>
					<i className="fa fa-user"></i>
				</ButtonCircle>
			</Navbar>
			<Switch>
				<Route path="/settings">
					<SettingsPage />
				</Route>
				<Route path="/add">
					<AddPage />
				</Route>
				<Route path="/profile">
					<ProfilePage />
				</Route>
				<Route path="/stuffs">
					<StuffPage />
				</Route>
			</Switch>
			<Addpage />
			<Menu />
			<Alert />
		</Router>
	);
}