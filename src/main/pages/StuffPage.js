import React from 'react';
import "./Page.scss";
import Nav from '../nav/Nav';
// import {CredentialsCard} from '../cards/crendentials/CredentialsCard';
// import {LinkField} from '../cards/link/LinkField';
// import {Card, CardBody} from '../cards/Card';
import {Card} from '../cards/CardAlt';
import {Alert} from "../alert/Alert";

import {Field, Hashtag, Button} from "../components/components";

// import Avatar from "../../res/avatar.jpg";

const fb_test = {
    header: {
        icon: "fa fa-lock",
        title: "Facebook credentials",
        info: "Today, at 14:53",
    },
    footer: ["facebook","pass","fb","mess","messenger"]
};

// const url_test = {
//     header: {
//         icon: "fa fa-link",
//         title: "My wbsite",
//     },
//     footer: ["cool","thebest","allmy"]
// };

export default class StuffPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            alert: {
                show: false,
            },
            nav: {
                items: [
                    {
                        title: "All stuffs",
                        icon: "fa fa-icons",
                        onClick: () => this.handleNavItemClick("All stuffs"),
                    },
                    {
                        title: "Recent",
                        icon: "fa fa-history",
                        onClick: () => this.handleNavItemClick("Recent"),
                        active: true,
                    },
                    {
                        isSectionDelimitator: true
                    },
                    {
                        title: "Credentials",
                        icon: "fa fa-lock",
                        onClick: () => this.handleNavItemClick("Credentials"),
                    },
                    {
                        title: "Links",
                        icon: "fa fa-link",
                        onClick: () => this.handleNavItemClick("Links"),
                    },
                    {
                        title: "Ideas",
                        icon: "far fa-lightbulb",
                        onClick: () => this.handleNavItemClick("Ideas"),
                    }
                ]
            },
            stuffs: [
                fb_test
            ]
        };
    }

    render() {
        return (
            <div className="page bg-lightt flex-fill">
                <div className="container pt-3">
                    <div className="row">
                        <div className="col-md-4">
                            <Nav items={this.state.nav.items} />
                        </div>
                        <div className="col-md-8">
                            
                            <Card>
                                <Card.Header>
                                    <Card.Header.Icon icon="fa fa-link" />
                                    <Card.Header.Title>Facebook crendentials</Card.Header.Title>
                                    <Card.Header.Opts>
                                        <Button.Circle size="sm">
                                            <i className="fa fa-info"></i>
                                        </Button.Circle>
                                    </Card.Header.Opts>
                                </Card.Header>
                                <Card.Body>
                                    <Field.Link href="http://pula.mea" onAlert={alert => this.handleAlert(alert)} />
                                    {/* <Field.Link href="http://eb.lan.ceg/" onAlert={alert => this.handleAlert(alert)} /> */}
                                    {/* <Field.Text text="Text" onAlert={alert => this.handleAlert(alert)} />
                                    <Field.Password text="Epta" onAlert={alert => this.handleAlert(alert)} /> */}
                                </Card.Body>
                                {/* <Card.Footer>
                                    {["epta","eblan"].map((item) => <Hashtag key={item}>{item}</Hashtag>)}
                                </Card.Footer> */}
                            </Card>

                            <Card>
                                <Card.Header>
                                    <Card.Header.Icon icon="fa fa-lightbulb" />
                                    <Card.Header.Title>My ideas</Card.Header.Title>
                                </Card.Header>
                                <Card.Body>
                                    <Field.Text text="saniok2000" onAlert={alert => this.handleAlert(alert)} />
                                    <Field.Password text="passwordcik" onAlert={alert => this.handleAlert(alert)}/>
                                </Card.Body>
                                <Card.Footer>
                                    {["epta","eblan"].map((item) => <Hashtag key={item}>{item}</Hashtag>)}
                                </Card.Footer>
                            </Card>

                            <Card>
                                <Card.Header>
                                    <Card.Header.Icon icon="fa fa-lightbulb" />
                                    <Card.Header.Title>My ideas</Card.Header.Title>
                                </Card.Header>
                                <Card.Body>
                                    <Field.Text text="saniok2000" onAlert={alert => this.handleAlert(alert)} />
                                    <Field.Password text="passwordcik" onAlert={alert => this.handleAlert(alert)}/>
                                </Card.Body>
                                <Card.Footer>
                                    {["epta","eblan"].map((item) => <Hashtag key={item}>{item}</Hashtag>)}
                                </Card.Footer>
                            </Card>

                            <Card>
                                <Card.Header>
                                    <Card.Header.Icon icon="fa fa-lightbulb" />
                                    <Card.Header.Title>My ideas</Card.Header.Title>
                                </Card.Header>
                                <Card.Body>
                                    <Field.Text text="saniok2000" onAlert={alert => this.handleAlert(alert)} />
                                    <Field.Password text="passwordcik" onAlert={alert => this.handleAlert(alert)}/>
                                </Card.Body>
                                <Card.Footer>
                                    {["epta","eblan"].map((item) => <Hashtag key={item}>{item}</Hashtag>)}
                                </Card.Footer>
                            </Card>

                            {/* <Card state={url_test}>
                                <CardBody>
                                    <Field.Link href="https://facebook.com" onAlert={alert => this.handleAlert(alert)}/>
                                </CardBody>
                            </Card> */}
                            {/* {this.state.stuffs.map((stuff) => {
                                return (
                                    <Card state={stuff} key={stuff.header.title}>
                                        <CredentialsCard onAlert={alert => this.handleAlert(alert)}/>
                                    </Card>
                                )
                            })} */}

                            {/* <Card state={fb_test}>
                                <CredentialsCard onAlert={alert => this.handleAlert(alert)}/>
                            </Card> */}
                        </div>
                    </div>
                </div>
                <Alert state={this.state.alert} onAlertHide={() => this.handleAlertHide()}/>
            </div>
        );
    }

    handleNavItemClick(itemTitle) {
        const state = this.state;
        
        state.nav.items.map((item) => {
            if (!item.isSectionDelimitator) {
                if (itemTitle === item.title) {
                    item.active = true;
                } else {
                    item.active = false;
                }
            }

            return item;
        })

        state.stuffs = this.getAllStuffs();

        this.setState(state);
    }

    handleAlert(text) {
        const state = this.state;
        state.alert = {
            show: true,
            content: text,
        };
        this.setState(state);
    }

    handleAlertHide() {
        const state = this.state;
        state.alert = {
            show: false,
        };
        this.setState(state);
    }

    getAllStuffs() {
        console.log("get all stuffs");
        const stuffs = [
            {
                header: {
                    icon: "fa fa-lock",
                    title: "Facebook credentials for my own cool website built in reacjs",
                    info: "Today, at 14:53",
                },
                footer: ["facebook","pass","fb","mess","messenger"]
            },
            {
                header: {
                    icon: "fa fa-link",
                    title: "React tutorial",
                    info: "Yesterday, at 09:35",
                },
                footer: ["react","tutorial"]
            },
            {
                header: {
                    icon: "fa fa-lightbulb",
                    title: "My idea for millions",
                    info: "Monday, at 21:09",
                },
                footer: ["idea","bussines", "millions", "reach"]
            }
        ];


        return stuffs;
        // const state = this.state;
        // state.stuffs = stuffs;
        // this.setState(state);
    }
}