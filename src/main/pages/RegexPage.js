import React from 'react';
import "./Page.scss";
import "../components/components.scss";
// import { ButtonCircle } from '../components/components';

export default class RegexPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            text: "",
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    render() {
        return (
            <div className="page bg-light flex-fill">
                <div className="container pt-3">
                    <div className="row">
                        <div className="col-md-2">
                            {/* <ButtonCircle onClick={() => {}}><i className="fa fa-pen"></i></ButtonCircle> */}
                        </div>
                        <div className="col-md-10">
                            <form onSubmit={this.handleSubmit}>
                                <textarea className="textarea" onChange={this.handleChange} cols="60" rows="20" placeholder="Write some lines"></textarea>
                                <input type="submit" value="Submit" />
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    parse(text) {
        let arr = text.split("\n");

        let maxLength = 0;
        let minLength = 99999;

        arr.map((i) => {
            if (maxLength < i.length) {
                maxLength = i.length
            }

            if (minLength > i.length) {
                minLength = i.length;
            }
            
            return i;
        })

        // console.log("Max -> " + maxLength);


        // phase I
        let phase1 = [[]];
        for (let i = 0; i < maxLength; i++) {
            arr.map((item) => {
                if (phase1[i] === undefined) {
                    phase1[i] = [];
                }

                if (item[i] !== undefined) {
                    if (phase1[i][item[i]] === undefined) {
                        phase1[i][item[i]] = item[i];
                    }
                }                

                return item;
            })
        }

        console.log("Phase 1");
        console.log(phase1);
        
        // phase II
        let phase2 = "";
        let i = 0;
        phase1.map((phase) => {
            phase = Object.keys(phase)
            if (phase.length > 1) {
                if (i >= minLength) {
                    phase2 += "[" + phase.join("") + "]?";    
                } else {
                    phase2 += "[" + phase.join("") + "]";
                }
            } else {
                if (i >= minLength) {
                    phase2 += phase[0] + "?";
                } else {
                    phase2 += phase[0];
                }
            }

            i++;
            return phase;
        })

        console.log("Phase 2");
        console.log(phase2);

    }

    handleSubmit(event) {
        this.parse(this.state.text);
        event.preventDefault();
    }

    handleChange(event) {
        this.setState({text: event.target.value});
    }
}