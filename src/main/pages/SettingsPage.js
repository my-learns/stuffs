import React from 'react';
// import Box from '../Box'
// import BoxHeader from '../BoxHeader'
import Nav from '../nav/Nav';

export default class SettingsPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            page: "Personal",
            nav: {
                items: [
                    {
                        title: "Personal",
                        icon: "fa fa-user",
                        active: true,
                        onClick: () => this.handleNavItemClick("Personal"),
                    },
                    {
                        title: "General",
                        icon: "fa fa-thumbs-up",
                        onClick: () => this.handleNavItemClick("General"),
                    },
                    {
                        title: "Privacy",
                        icon: "fa fa-shield-alt",
                        onClick: () => this.handleNavItemClick("Privacy"),
                    }
                ]
            }
        };
    }

    render() {
        return (
            <div className="page bg-lightt flex-fill">
                <div className="container pt-3">
                    <div className="row">
                        <div className="col-md-4 pb-3" style={{minWidth: "200px"}}>
                            <Nav items={this.state.nav.items}></Nav>
                        </div>
                        <div className="col-md-8">
                            {/* <Box>
                                <BoxHeader>
                                    {this.state.page}
                                </BoxHeader>
                            </Box> */}
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    handleNavItemClick(itemTitle) {
        const state = this.state;
        
        state.nav.items.map((item) => {
            if (!item.isSectionDelimitator) {
                if (itemTitle === item.title) {
                    item.active = true;
                    state.page = itemTitle;
                } else {
                    item.active = false;
                }
            }

            return item;
        })

        this.setState(state);
    }
}