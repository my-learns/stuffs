import React from 'react';
import './Card.scss'

export function Card (props){
    if (props.state) {
        if (props.state.body) {
            return (
                <div className="_card">
                    <CardHeader state={props.state.header} />
                    <CardBody state={props.state.body} />
                    <CardFooter state={props.state.footer} />
                </div>
            );
        } else {
            return (
                <div className="_card">
                    <CardHeaderSimple state={props.state.header} />
                        {props.children}
                    <CardFooter state={props.state.footer} />
                </div>
            );
        }
    } else if (props.children) {
        return (
            <div className="_card">
                {props.children}
            </div>
        );
    }
}

export function CardHeader(props) {
    if (props.children) {
        return <div className="_card-header">{props.children}</div>;
    } else if (props.state) {
        return (
            <div className="_card-header">
                <div>
                    <div className={"_card-header-icon" + (props.state.avatar ? " _card-header-img" : "")} style={{backgroundImage: props.state.avatar ? "url(" + props.state.avatar + ")" : "none"}}>
                        <i className={!props.state.avatar ? (props.state.icon ? props.state.icon : "fa fa-circle") : "fa fa-circlle"}></i>
                    </div>
                </div>
                <div className="d-flex flex-row w-100">
                    <div className="_card-header-content flex-fill">
                        <div className="_card-header-content-title">{props.state.title}</div>
                        <div className="_card-header-content-info">{props.state.info}</div>
                    </div>
                    <div className="_card-header-opts">
                        <div className="_card-header-opts-opt">
                            <i className="far fa-star"></i>
                        </div>
                        <div className="_card-header-opts-opt">
                            <i className="fa fa-ellipsis-h"></i>
                        </div>
                    </div>
                </div>
            </div>
        );
    } else {
        return <div className="_card-header">Empty card header</div>;
    }
}

export function CardHeaderSimple(props) {
    if (props.children) {
        return <div className="_card-header">{props.children}</div>;
    } else if (props.state) {
        return (
            <div className="_card-header-simple">
                <div className={"_card-header-simple-icon" + (props.state.avatar ? " _card-header-img" : "")} style={{backgroundImage: props.state.avatar ? "url(" + props.state.avatar + ")" : "none"}}>
                    <i className={!props.state.avatar ? (props.state.icon ? props.state.icon : "fa fa-circle") : "fa fa-circlle"}></i>
                </div>
                <div className="_card-header-simple-title flex-fill">
                    {props.state.title}
                </div>
            </div>
        );
    } else {
        return <div className="_card-header">Empty card header</div>;
    }
}

export function CardBody(props) {
    if (props.children) {
        return <div className="_card-body">{props.children}</div>;
    } else if (props.state) {
        return <div className="_card-body">Body with state</div>;
    } else {
        return <div className="_card-body">Epty card body</div>;
    }   
}

export function CardFooter(props) {
    if (props.children) {
        return <div className="_card-footer">{props.children}</div>
    } else if (props.state) {
        return (
            <div className="_card-footer">
                {props.state.map((hashtag) => <Hashtag key={hashtag}>#{hashtag}</Hashtag>)}
                {/* <Hashtag>#hashtag</Hashtag>
                <Hashtag>#nieniipoihui</Hashtag>
                <Hashtag>#covrig19</Hashtag>
                <Hashtag>#win1</Hashtag>
                <Hashtag>#nunah</Hashtag>
                <Hashtag>#jorabaton</Hashtag>
                <Hashtag>#danuna</Hashtag> */}
            </div>
        );
    } else {
        return <div className="_card-footer">Emtpy card footer</div>;
    }
}

export function Hashtag(props) {
    return (
        <span className="_card-hashtag">
            {props.children}
        </span>
    );
}