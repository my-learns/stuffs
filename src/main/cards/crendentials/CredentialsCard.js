import React from 'react';
import Clipboard from 'react-clipboard.js';
import '../Card.scss'
import {ButtonCircle} from '../../components/components'

export class CredentialsCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            field: {
                text: "Eptanunahui",
                isPassword: true,
                opts: [
                ]
            }
        }
    }

    render() {
        return (
            <div className="pt-3 pb-3">
                <div className="d-flex justify-content-center">
                    <div className="ufield-box">
                        <UField state={{text: "saniok2000"}} onAlert={alert => this.props.onAlert(alert)}/>
                        <UField state={this.state.field} onAlert={alert => this.props.onAlert(alert)} onAlertHide={() => this.props.onAlertHide()}/>
                    </div>
                </div>
            </div>
        );
    }

    handleClick() {
        const state = this.state;
        state.field.isPassword = !state.field.isPassword;
        this.setState(state);
    }
}

export class UField extends React.Component {
    constructor(props) {
        super(props);
        this.state = props.state;
    } 

    render() {
        if (this.state) {
            let text = this.state.text;
            let showHidePassOpt;

            if (this.state.isPassword !== undefined) {
                if (this.state.isPassword) {
                    // showHidePassOpt = <div className={"_card-header-opts-opt fa fa-eye-slash"} onClick={() => this.handleEyeClick()}></div>;
                    showHidePassOpt = <ButtonCircle onClick={() => this.handleEyeClick()}><i className="fa fa-eye-slash"></i></ButtonCircle>;
                } else {
                    // showHidePassOpt = <div className={"_card-header-opts-opt fa fa-eye"} onClick={() => this.handleEyeClick()}></div>;
                    showHidePassOpt = <ButtonCircle onClick={() => this.handleEyeClick()}><i className="fa fa-eye"></i></ButtonCircle>;
                }
            }

            return (
                <div className="ufield">
                    <div className="ufield-text">
                        {this.state.isPassword === true ? <i>&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;</i> : text}
                    </div>
                    {showHidePassOpt}
                    <Clipboard className="btn-circle fa fa-copy" style={{border: "none"}} data-clipboard-text={this.state.text} onClick={() => this.props.onAlert("Copied to clipboard")}/>
                </div>
            );
        } else {
            return (
                <div className="ufield">
                    <div className="ufield-text">{this.props.children}</div>
                    <div className="_card-header-opts-opt fa fa-copy"></div>
                </div>
            );
        }
    }

    handleEyeClick() {
        const state = this.state;
        state.isPassword = !state.isPassword;
        this.setState(state);
    }
}