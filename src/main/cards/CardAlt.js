import React from 'react';
import './Card.scss'
import {Hashtag} from '../components/components';

export class Card extends React.Component {
    render() {
        return <div className={"_card" + (this.props.blue ? " _card-blue" : "")}>{this.props.children}</div>;
    }
}

Card.Header = class extends React.Component {
    render() {
        if (this.props.children) {
            return <div className="_card-header-simple">{this.props.children}</div>;
        } else if (this.props.state) {
            return (
                <div className="_card-header-simple">
                    <div className="_card-header-simple-icon">
                        <i className={this.props.state.icon ? this.props.state.icon : "fa fa-circle"}></i>
                    </div>
                    <div className="_card-header-simple-title flex-fill">
                        {this.props.state.title}
                    </div>
                </div>
            );
        } else {
            return <div className="_card-header-simple">Empty card header</div>;
        }
    }
}

Card.Header.Icon = (props) => {
    return (
        <div className="_card-header-simple-icon">
            <i className={props.icon ? props.icon : "fa fa-circle"}></i>
        </div>
    );
}

Card.Header.Title = (props) => {
    return <div className="_card-header-simple-title flex-fill">{props.children}</div>
}

Card.Header.Opts = (props) => {
    return <div className="_card-header-ops">{props.children}</div>
}

Card.Footer = (props) => {
    if (props.children) {
        return <div className="_card-footer">{props.children}</div>
    } else if (props.state) {
        return (
            <div className="_card-footer">
                {props.state.map((hashtag) => <Hashtag key={hashtag}>#{hashtag}</Hashtag>)}
            </div>
        );
    } else {
        return <div className="_card-footer">Emtpy card footer</div>;
    }
}

Card.Body = (props) => {
    if (props.children) {
        return <div className="_card-body">{props.children}</div>;
    } else if (props.state) {
        return <div className="_card-body">Body with state</div>;
    } else {
        return <div className="_card-body">Epty card body</div>;
    }
}