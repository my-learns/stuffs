import React from 'react';
import NavItem from './NavItem';
import NavSectionDelimitator from './NavSectionDelimitator';
import './Nav.scss';

export default function Nav(props) {
    if (props.children) {
        return (
            <div className="lnav">
                {props.children}
            </div>
        );
    } else if (props.items) {
        return (
            <div className="lnav">
                {props.items.map((item) => {
                    if (item.isSectionDelimitator) {
                        return <NavSectionDelimitator key=""/>
                    } else {
                        if (item.active) {
                            return <NavItem active={true} icon={item.icon} onClick={() => item.onClick()} key={item.title}>{item.title}</NavItem>
                        } else {
                            return <NavItem icon={item.icon} onClick={() => item.onClick()} key={item.title}>{item.title}</NavItem>
                        }
                    }
                })}
            </div>
        );
    } else {
        return <div>Empty nav</div>
    }
}