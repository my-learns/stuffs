import React from 'react';

export default function NavItem(props) {
    return (
        <div className={(props.active ? "bg-primary text-light " : "") + "lnav-item p-3 pl-3 d-flex"} onClick={() => props.onClick()}>
            <h4 className="m-0" style={{width: "40px"}}>
                <i className={props.icon + (props.active ? " text-light" : " text-primary")}></i>
            </h4>
            <span className="align-items-center" style={{lineHeight: "28px"}}>{props.children}</span>
        </div>
    );
}