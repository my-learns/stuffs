import React from 'react';
import "./Nav.scss";

export default function NavSectionDelimitator(props) {
    return (
        <hr className="mx-3 mt-1 mb-1" />
    );
}