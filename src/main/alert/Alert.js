import React from 'react';
import {Button} from "../components/components";
import './Alert.scss';

export class Alert extends React.Component {
    constructor(props) {
        props.state.closed = false;
        super(props);
        this.state = {};
    }

    render() {
        if (this.props.state.show && this.props.state.closed) {
            this.props.state.closed = false;
        }

        return (
            <div className={"notif-container" + (this.props.state.show ? " notif-animation" : (this.props.state.closed ? " notif-hide" : ""))} onAnimationEnd={this.props.onAlertHide}>
                <div className="notif">
                    <div className="notif-content">
                        {this.props.state.content}
                    </div>
                    <div>
                        <Button.Circle.Light onClick={() => this.handleCloseClick()}>
                            <i className="fa fa-times"></i>
                        </Button.Circle.Light>
                    </div>
                </div>
            </div>
        );
    }

    handleCloseClick() {
        this.props.state.show = false;
        this.props.state.closed = true;
        this.setState({});
    }
}