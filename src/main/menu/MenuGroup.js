import React from 'react';
import MenuGroupItem from './MenuGroupItem';
import MenuItem from './MenuItem';

export default class MenuGroup extends React.Component {
    constructor(props) {
        super(props);
        this.state = this.props.state;
        this.state.onClick = () => this.handleClick();
        if (!this.state.active) this.state.active = false;
    }
    
    render() {
        return (
            <div>
                <MenuItem state={this.state}/>
                <div className={"pl-4"+(this.state.active ? " bg-darker" : "")} style={{overflow: "hidden", transition: ".4s", height: this.state.active ? (this.state.items.length*36)+"px" : "0px"}}>
                    {this.state.items.map((item) => {
                        if (!item.icon) item.icon = "fa fa-angle-right";
                        return (<MenuGroupItem state={item} key={item.title}/>)})
                    }
                </div>
            </div>
        );
    }

    handleClick() {
        const state = this.state;
        state.active = !state.active;
        this.setState(state);
    }
}