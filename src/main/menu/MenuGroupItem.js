import React from 'react';

export default class MenuGroupItem extends React.Component {
    render() {
        return (
            <h6 className="menu-item font-weight-light p-2 pl-4">
                <i className={this.props.state.icon + ' mr-4'}></i>
                <span>{this.props.state.title}</span>
            </h6>
        );
    }
}