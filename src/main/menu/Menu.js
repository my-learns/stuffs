import React from 'react';
import MenuItem from './MenuItem';
import './Menu.scss';
import { Link } from 'react-router-dom';

export default class Menu extends React.Component {
    render() {
        return (
            <div>
                <div style={{position: "absolute", height: "100vh", backgroundColor: "#00000066", top: "0", left: "0", width: "100vw", transition: ".6s"}} className={this.props.state.active ? "" : "d-none"} onClick={this.props.state.switchHandler}></div>
                <div className="bg-dark m-0 p-0 text-light h-100 position-absolute" style={{width: "250px", top: "0px", left: this.props.state.active ? "0px" : "-250px", transition: ".4s"}}>
                    <h5 className="font-weight-light m-0 p-3 px-3 pointer" onClick={this.props.state.switchHandler}>
                        <i className="fa fa-bars mr-3"></i>
                        <span>My Stuff</span>
                    </h5>
                    <hr className="m-0" />
                    <div className="d-flex flex-column">
                        {this.props.state.items.map((item) => {
                            return (
                                <Link to={item.path}>
                                    <MenuItem state={item} key={item.title}/>
                                </Link>
                            );
                        })}
                    </div>
                </div>
            </div>
        );
    }
}