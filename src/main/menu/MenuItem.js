import React from 'react';

export default class MenuItem extends React.Component {
    render() {
        return (
            <h4 className="menu-item font-weight-light p-3 pl-4">
                <i className={this.props.state.icon + ' mr-4'}></i>
                <span>{this.props.state.title}</span>
            </h4>
        );
    }
}