import React from 'react';
import Clipboard from 'react-clipboard.js';
import './components.scss';
import { Link } from 'react-router-dom';

// export function ButtonCircle(props) {
//     return (
//         <div className={"btn-circle" + (props.light ? " btn-circle-light" : "")} onClick={() => props.onClick()}>
//             {props.children}
//         </div>
//     );
// }

export class Field extends React.Component {
    render() {
        return (
            <div className="ufield">
                <div className="ufield-text">
                    {this.props.children ? this.props.children : (this.props.text ? this.props.text : "empty field")}
                </div>
            </div>
        );
    }
}

Field.Link = (props) => {
    return (
        <div className="ufield ufield-link">
            {/* <div className="ufield-icon fab fa-facebook mr-2 text-primary"></div> */}
            <div>
                <img className="ufield-icon mr-2" src="https://z-p3-static.xx.fbcdn.net/rsrc.php/yD/r/d4ZIVX-5C-b.ico" alt=""/>
            </div>
            <div className="ufield-text" title={props.href}>
                {props.href}
            </div>
            <Button.Circle.Copy value={props.text} onAlert={alert => props.onAlert(alert)} />
            <Button.Circle.Link href={props.href} />
        </div>
    );
};

Field.Text = (props) => {
    return (
        <div className="ufield">
            <div className="ufield-text" title={props.text}>
                {props.text}
            </div>
            <Button.Circle.Copy value={props.text} onAlert={alert => props.onAlert(alert)} />
        </div>
    )
}

Field.Password = class extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            show: this.props.show,
            text: this.props.text
        };
    }

    render() {
        return (   
            <div className="ufield">
                <div className="ufield-text">
                    {this.state.show ? this.state.text : <i>&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;</i>}
                </div>
                {this.state.show ? <Button.Circle.Copy value={this.state.text} onAlert={alert => this.props.onAlert(alert)} /> : ""}
                <Button.Circle.ShowHide show={this.state.show} onClick={() => this.handleShowHide()}/>
            </div>
        );
    }

    handleShowHide() {
        const state = this.state;
        state.show = !state.show;
        this.setState(state);
    }
}

export class Button extends React.Component {
    render() {
        return <div>Button</div>
    }
}

Button.Circle = class extends React.Component {
    render() {
        if (this.props.path) {
            return (
                <Link to={this.props.path} onClick={this.props.onClick}
                className={"btn-circle" 
                + (this.props.light ? " btn-circle-light" : "")
                + (this.props.size ? " btn-circle-" + this.props.size : "")
                + (this.props.margin ? " m-1" : "")
                }>
                    {this.props.children}
                </Link>
            );
        } else {
            return (
                <div className={"btn-circle" + (this.props.light ? " btn-circle-light" : "") + (this.props.size ? " btn-circle-" + this.props.size : "") + (this.props.margin ? " m-1" : "")} onClick={() => this.props.onClick()}>
                    {this.props.children}
                </div>
            );
        }
    }
}

Button.Circle.Light = (props) => {
    return <div className="btn-circle btn-circle-light" onClick={() => props.onClick()}>{props.children}</div>
}

Button.Circle.Copy = (props) => {
    return <Clipboard title="Copy" className="btn-circle fa fa-copy" style={{border: "none"}} data-clipboard-text={props.value} onClick={() => props.onAlert("Copied to clipboard")}/>
}

Button.Circle.Link = (props) => {
    return <a href={props.href} title="Open in new window" target="_blank" rel="noopener noreferrer" className="btn-circle fa fa-external-link-alt"> </a>
}

Button.Circle.ShowHide = (props) => {
    if (props.show) {
        return <div className="btn-circle fa fa-eye" onClick={() => props.onClick()} />
    } else {
        return <div className="btn-circle fa fa-eye-slash" onClick={() => props.onClick()} />
    }
}

export class Hashtag extends React.Component {
    render() {
        return <span className="_card-hashtag">#{this.props.children}</span>;
    }
}