import React from 'react';
import "./Navbar.scss";

export default class Navbar extends React.Component {
    render() {
        return (
            <div className="bg-dark text-light d-flex">
                <h5 className="font-weight-light m-0 p-3 px-3 pointer" onClick={this.props.onClick}>
                    <i className="fa fa-bars mr-3"></i>
                    <span>My Stuff</span>
                </h5>

                <div className="ml-auto pr-3 d-flex justify-content-center align-items-center position-relative">
                    <input type="text" className="navbar-search-input" placeholder="Search"/>
                    <i className="fa fa-search position-absolute p-1 pr-3 navbar-search-icon"></i>
                </div>
            </div>
        );
    }
}