import React from 'react';
import ProfilePage from './pages/ProfilePage';
import SettingsPage from './pages/SettingsPage';
import StuffPage from './pages/StuffPage';
import RegexPage from './pages/RegexPage';

export default class Body extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            page: "Regex"
        };
    }
    render() {

        if (this.props.page === "Profile") {
            return (
                <ProfilePage />
            );
        } else if (this.props.page === "Settings") {
            return (
                <SettingsPage />
            );
        } else if (this.props.page === "Stuff") {
            return (
                <StuffPage />
            );
        } else if (this.props.page === "Regex") {
            return (
                <RegexPage />
            );
        } else {
            return (
                <div>Page not found</div>
            );
        }
    }
}