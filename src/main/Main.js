import React from 'react';
import Menu from './menu/Menu';
import Navbar from './navbar/Navbar';
// import Body from './Body';
import { Switch, Route } from 'react-router-dom';
import SettingsPage from './pages/SettingsPage';
import ProfilePage from './pages/ProfilePage';
import StuffPage from './pages/StuffPage';

export default class Main extends React.Component {
    constructor(props) {
        super(props);
        console.log(props);
        this.state = {
            menu: {
                active: false,
                switchHandler: () => this.handleClick(),
                items: [
                    {
                        title: "Profile",
                        icon: "fa fa-user-circle",
                        onClick: () => this._handleMenuItemClick(),
                        path: "/profile",
                    },
                    {
                        title: "Stuff",
                        icon: "fa fa-icons",
                        onClick: () => this._handleMenuItemClick(),
                        path: "/stuffs",
                    },
                    {
                        title: "Regex",
                        icon: "fa fa-bolt",
                        onClick: () => this._handleMenuItemClick(),
                        path: "/regex",
                    },
                    {
                        title: "Settings",
                        icon: "fa fa-cog",
                        onClick: () => this._handleMenuItemClick(),
                        path: "/settings",
                    },
                    {
                        title: "Log out",
                        icon: "fa fa-sign-out",
                        onClick: () => {} 
                    }
                ]
            },
            page: (this.props.page ? this.props.page : "Profile")
        };
    }

    // render() {
    //     return (
    //         <div style={{height: "100vh", overflowY: "hiddenn"}}>
    //             <div style={{heightt: "100vh", overflowY: "hiddenn"}} className="d-flex flex-column">
    //                 <Navbar onClick={() => this.handleClick()} />
    //                 <Body page={this.state.page}/>
    //             </div>
    //             <Menu state={this.state.menu} />
    //         </div>
    //     );
    // }

    render() {
        return (
            <div style={{height: "100vh", overflowY: "hiddenn"}}>
                <div style={{heightt: "100vh", overflowY: "hiddenn"}} className="d-flex flex-column">
                    <Navbar onClick={() => this.handleClick()} />
                    <Switch>
                        <Route path="/settings">
                            <SettingsPage />
                        </Route>
                        <Route path="/profile">
                            <ProfilePage />
                        </Route>
                        <Route path="/stuffs">
                            <StuffPage />
                        </Route>
                    </Switch>
                </div>
                <Menu state={this.state.menu} />
            </div>
        );
    }

    handleClick() {
        const state = this.state;
        state.menu.active = !state.menu.active;
        this.setState(state);
    }

    handleProfileClick() {
        const state = this.state;
        state.page = "Profile";
        state.menu.active = false;
        this.setState(state);
    }

    handleSettingsClick() {
        const state = this.state;
        state.page = "Settings";
        state.menu.active = false;
        this.setState(state);
    }

    handleMenuItemClick(page) {
        const state = this.state;
        // state.page = page;
        state.menu.active = false;
        this.setState(state);
    }

    _handleMenuItemClick() {
        const state = this.state;
        state.menu.active = false;
        this.setState(state);
    }
}