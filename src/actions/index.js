export const ALERT = {
    SHOW: "ALERT_SHOW",
    HIDE: "ALERT_HIDE",
    CLOSE: "ALERT_CLOSE",
}

export const showAlert = text => ({
    type: ALERT.SHOW,
    text
})

export const hideAlert = () => ({
    type: ALERT.HIDE
})

export const closeAlert = () => ({
    type: ALERT.CLOSE
})

export const MENU = {
    SHOW: "MENU_SHOW",
    HIDE: "MENU_HIDE"
}

export const showMenu = () => ({
    type: MENU.SHOW
})

export const hideMenu = () => ({
    type: MENU.HIDE
})

export const DRP_SHOW = "DRP_SHOW"
export const DRP_HIDE = "DRP_HIDE"
export const DRP_SHOW_HIDE = "DRP_SHOW_HIDE"
export const DRP_LEFT_SHOW_HIDE = "DRP_LEFT_SHOW_HIDE"


export const showHideDrp = () => ({
    type: DRP_SHOW_HIDE
})

export const showDrp = () => ({
    type: DRP_SHOW
})

export const hideDrp = () => ({
    type: DRP_HIDE
})

export const showHideDrpLeft = () => ({
    type: DRP_LEFT_SHOW_HIDE
})

export const COVER_SHOW = "COVER_SHOW"
export const COVER_HIDE = "COVER_HIDE"

export const showCover = () => ({
    type: COVER_SHOW
})

export const hideCover = () => ({
    type: COVER_HIDE
})


export const ADDPAGE_SHOW = "ADDPAGE_SHOW"
export const ADDPAGE_HIDE = "ADDPAGE_HIDE"
export const ADDPAGE_TYPE = {
    LINK: "ADDPAGE_LINK",
}
export const ADDPAGE_ADD_FIELD = "ADDPAGE_ADD_FIELD"
export const ADDPAGE_REMOVE_FIELD = "ADDPAGE_REMOVE_FIELD"
export const ADDPAGE_EDIT_FIELD = "ADDPAGE_EDIT_FIELD"

export const showAddPage = () => ({
    type: ADDPAGE_SHOW
})

export const hideAddPage = () => ({
    type: ADDPAGE_HIDE
})

export const addPageEditField = (field) => ({
    type: ADDPAGE_EDIT_FIELD,
    field: field
})

export const addPageAddField = (field) => ({
    type: ADDPAGE_ADD_FIELD,
    field: field
})

export const addPageRemoveField = (id) => ({
    type: ADDPAGE_REMOVE_FIELD,
    id: id
})


export const FIELD_TYPE = {
    TEXT: 0,
    LINK: 1,
    PASSWORD: 2,
}