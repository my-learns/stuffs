import React from 'react'
import PropTypes from 'prop-types'
import "../main/navbar/Navbar.scss"
import { Button } from '../main/components/components'
import { DropLeft } from '../containers/Dropleft'

const Navbar = ({children, onMenuShow, onAddPageShow, dropdownActive, dropleftActive, onDropleftClick, onAddClick}) => (
    <div className="bg-dark text-light d-flex">
        <h5 className="font-weight-light m-0 p-3 px-3 pointer" onClick={onMenuShow}>
            <i className="fa fa-bars mr-3"></i>
            <span>My Stuff</span>
        </h5>

        <div className="ml-auto pr-3 d-flex justify-content-center align-items-center position-relative">
        
            <div className="mr-2">
                <DropLeft light={true} active={dropleftActive} onClick={onDropleftClick} icon="fa fa-plus">
                    <DropLeft.Item onClick={onAddPageShow} light={true} active={dropleftActive} icon="fa fa-lock" pos={1} />
                    <DropLeft.Item path="/add/links" light={true} active={dropleftActive} icon="fa fa-link" pos={2} />
                    <DropLeft.Item path="/add/ideas" light={true} active={dropleftActive} icon="fa fa-lightbulb" pos={3} />
                </DropLeft>
            </div>

            <div className="drpdown d-none">
                <div className="drpdown-button">
                    <Button.Circle light={true} margin={true} onClick={onAddClick}>
                        <i className="fa fa-plus"></i>
                    </Button.Circle>
                </div>
                <div className={"drpdown-menu " + (dropdownActive ? "drp-show" : "drp-hide")}>

                    <div className="position-relative">

                        <div className={"drp-menu-item " +  (dropdownActive ? "drp-btn-show" : "drp-btn-hide")}>epta
                            {/* <div className="drp-btn">
                                <div className="drp-btn-icon">
                                    <i className="fa fa-user"></i>
                                </div>
                                <div className="drp-btn-text">Epta</div>
                            </div> */}
                        </div>

                        <div className={"drp-menu-item " +  (dropdownActive ? "drp-btn-show" : "drp-btn-hide")}>epta
                            {/* <div className="drp-btn">
                                <div className="drp-btn-icon">
                                    <i className="fa fa-user"></i>
                                </div>
                                <div className="drp-btn-text">Epta</div>
                            </div> */}
                        </div>
                    
                    </div>

                </div>
            </div>

            <input type="text" className="navbar-search-input" placeholder="Search"/>
            <i className="fa fa-search position-absolute p-1 pr-3 navbar-search-icon"></i>
        </div>
    </div>
)

Navbar.propTypes = {
    onMenuShow: PropTypes.func.isRequired,
    children: PropTypes.node
}

export default Navbar