import React from 'react'
import './index.scss'
// import { Button } from '../../main/components/components'
import Button from '../Button_'

class EField extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: this.props.text
        }
        this.handleChange = this.handleChange.bind(this);
    }

    // onChange={() => this.props.onChange({value: this})}
    render() {
        return (
            <div className="field field-md">
                <div className="field-text">
                    <input 
                        type="text" 
                        ref="input" 
                        placeholder="Type something" 
                        onBlur={() => this.props.handleBlur({id: this.props.id, value: this.state.value})} 
                        className="field-input" 
                        value={this.state.value} 
                        onChange={() => this.handleChange()}/>
                </div>
                <div className="field-opts">
                    <Button.Circle.Close onClick={this.props.onRemove}/>
                </div>
            </div>
        )
    }

    handleChange() {
        this.setState({value: this.refs.input.value})
    }
}

EField.Link = (props) => {
    return (
        <div className="field field-md">
            <div className="field-text">
                <input type="text" placeholder="Type something" className="field-input" value={props.href} />
            </div>
            <div className="field-opts">
                <Button.Circle.Close onClick={props.onRemove} />
            </div>
        </div>
    )
}

export default EField;