import React from 'react'

export function ButtonXL(props) {
    return (
        <div className="button-xl-wrapper flex-fill">
            <div className="button-xl">
                <i className={"button-xl-icon " + (props.icon ? props.icon : "fa fa-circle")}></i>
                <span>{props.children}</span>
            </div>
        </div>
    )
}