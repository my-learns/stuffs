import React from 'react'
import './index.scss'
import { Button } from '../../main/components/components'

class Field extends React.Component {
    render = () => (
        <div className="field field-md">
            <div className="field-text">{this.props.text}</div>
            <div className="field-opts">
                <Button.Circle.Copy value={this.props.text} onAlert={this.props.onAlert}></Button.Circle.Copy>
            </div>
        </div>
    )
}

Field.Link = (props) => (
    <div className="field field-md">
        <div className="field-text">{props.href}</div>
        <div className="field-opts">
            <Button.Circle.Copy value={props.href} onAlert={props.onAlert}></Button.Circle.Copy>
            <Button.Circle.Link href={props.href} onAlert={props.onAlert}></Button.Circle.Link>
        </div>
    </div>
)

export default Field;