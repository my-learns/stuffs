import React from 'react'
import PropTypes from 'prop-types'
import MenuItem from '../main/menu/MenuItem'
import '../main/menu/Menu.scss';
import { Link } from 'react-router-dom';

const Menu = ({active, items, onHide}) => (
    <div>
        <div className={"cover cover-" + (active ?  "active" : "inactive")} onClick={onHide}></div>
        <div className={"bg-dark text-light menu menu-" + (active ?  "active" : "inactive")}>
            <h5 className="font-weight-light m-0 p-3 px-3 pointer" onClick={onHide}>
                <i className="fa fa-bars mr-3"></i>
                <span>My Stuff</span>
            </h5>
            <hr className="m-0" />
            <div className="d-flex flex-column">
            {items.map((item) => {
                return (
                    <Link to={item.path} onClick={onHide}>
                        <MenuItem state={item} key={item.title}/>
                    </Link>
                );
            })}
            </div>
        </div>
        
    </div>
)

Menu.propTypes = {
    active: PropTypes.bool.isRequired,
    onHide: PropTypes.func.isRequired
}

export default Menu