import React from 'react'
import PropTypes from 'prop-types'

const Button = ({light, size, children, onClick}) => (
    <div className={"btn-circle" + (light ? " btn-circle-light" : "") + (size ? " btn-circle-" + size : "")} onClick={onClick}>
        {children}
    </div>
)

Button.Circle.propTypes = {
    light: PropTypes.bool,
    size: PropTypes.string,
    children: PropTypes.node.isRequired,
    onClick: PropTypes.func.isRequired
}

// export default ButtonCircle



