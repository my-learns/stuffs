import React from 'react'
import { showAddPage, hideAddPage, showAlert } from '../actions'
import { connect } from 'react-redux'
import Cover from './Cover'
import { Card } from '../main/cards/CardAlt'
import { Button, Field } from '../main/components/components'
// import { Field}

const AddPage = ({active, data, onHide, onAlert}) => (
    <div>
        <Cover active={active} onClick={onHide}>
            <Card>
                <Card.Header>
                    <Card.Header.Icon icon={data.icon} />
                    <Card.Header.Title>{data.title}</Card.Header.Title>
                    <Card.Header.Opts>
                        <Button.Circle size="sm">
                            <i className="fa fa-info"></i>
                        </Button.Circle>
                    </Card.Header.Opts>
                </Card.Header>
                <Card.Body>
                    {data.fields.map((field) => (
                        <Field.Link href={field.text} onAlert={alert => onAlert(alert)} />
                    ))}

                    {/* <Field.Link href="http://pula.meaasdasfasgwfe" onAlert={alert => onAlert(alert)} /> */}
                    {/* <Field.Link href="http://eb.lan.ceg/" onAlert={alert => this.handleAlert(alert)} /> */}
                    {/* <Field.Text text="Text" onAlert={alert => this.handleAlert(alert)} />
                    <Field.Password text="Epta" onAlert={alert => this.handleAlert(alert)} /> */}
                </Card.Body>
                {/* <Card.Footer>
                    {["epta","eblan"].map((item) => <Hashtag key={item}>{item}</Hashtag>)}
                </Card.Footer> */}
            </Card>
        </Cover>
    </div>
)



const mapStateToProps = state => ({
    ...state.addpage
})

const mapDispatchToProps = dispatch => ({
    onShow: () => dispatch(showAddPage()),
    onHide: () => dispatch(hideAddPage()),
    onAlert: alert => dispatch(showAlert(alert))
})

export default connect(mapStateToProps, mapDispatchToProps)(AddPage)