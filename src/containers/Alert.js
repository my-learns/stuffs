import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {Button} from "../main/components/components";
import '../main/alert/Alert.scss';
import { hideAlert } from '../actions';

const Alert = ({text, active, onHide}) => (
    <div className={"notif-container" + (active ? " notif-animation" : "")} onAnimationEnd={onHide}>
        <div className="notif">
            <div className="notif-content">
                {text}
            </div>
            <div>
                <Button.Circle.Light onClick={() => onHide()}>
                    <i className="fa fa-times"></i>
                </Button.Circle.Light>
            </div>
        </div>
    </div>
)

Alert.propTypes = {
    active: PropTypes.bool.isRequired,
    text: PropTypes.string,
    onHide: PropTypes.func.isRequired
}

const mapStateToProps = state => state.alert

const mapDispatchToProps = dispatch => ({
    onHide: () => dispatch(hideAlert())
})

export default connect(mapStateToProps, mapDispatchToProps)(Alert);