import React from 'react'
import PropTypes from 'prop-types'

export class Nav extends React.Component {
    render() {
        return (
            <div className="lnav">{this.props.children}</div>
        )
    }
}

Nav.Item = ({icon, children, onClick}) => (
    <div className={(props.active ? "bg-primary text-light " : "") + "lnav-item p-3 pl-3 d-flex"} onClick={() => props.onClick()}>
        <h4 className="m-0" style={{width: "40px"}}>
            <i className={props.icon + (props.active ? " text-light" : " text-primary")}></i>
        </h4>
        <span className="align-items-center" style={{lineHeight: "28px"}}>{props.children}</span>
    </div>
)

Nav.Item.propTypes = {
    icon: PropTypes.string,
    children: PropTypes.node.isRequired,
    onClick: PropTypes.func.isRequired 
}