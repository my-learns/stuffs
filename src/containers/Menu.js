import { connect } from 'react-redux'
import { hideMenu } from '../actions'
import Menu from '../components/Menu'

const mapStateToProps = state => ({
    ...state.menu,
    items: [
        {
            title: "Profile",
            icon: "fa fa-user-circle",
            onClick: () => this._handleMenuItemClick(),
            path: "/profile",
        },
        {
            title: "Stuff",
            icon: "fa fa-icons",
            onClick: () => this._handleMenuItemClick(),
            path: "/stuffs",
        },
        {
            title: "Regex",
            icon: "fa fa-bolt",
            onClick: () => this._handleMenuItemClick(),
            path: "/regex",
        },
        {
            title: "Settings",
            icon: "fa fa-cog",
            onClick: () => this._handleMenuItemClick(),
            path: "/settings",
        },
        {
            title: "Log out",
            icon: "fa fa-sign-out",
            onClick: () => {} 
        }
    ]
})

const mapDispatchToProps = dispatch => ({
    onHide: () => dispatch(hideMenu()),
    // onHideCover: () => dispatch(hideCover())
})

export default connect(mapStateToProps, mapDispatchToProps)(Menu);