import React from 'react'
import { connect } from 'react-redux'
import "../main/components/components.scss"
import PropTypes from 'prop-types'
// import { showAlert } from '../actions'

const ButtonCircle = ({light, size, children, onClick}) => (
    <div className={"btn-circle" + (light ? " btn-circle-light" : "") + (size ? " btn-circle-" + size : "")} onClick={onClick}>
        {children}
    </div>
)

ButtonCircle.propTypes = {
    light: PropTypes.bool,
    size: PropTypes.string,
    children: PropTypes.node.isRequired,
    onClick: PropTypes.func.isRequired
}

const mapStateToProps = state => (state)

const mapDispatchToProps = (dispatch, props) => ({
    onClick: () => dispatch(props.onClick)
})

export default connect(mapStateToProps, mapDispatchToProps)(ButtonCircle)