import { connect } from 'react-redux'
import { showMenu, showHideDrp, showHideDrpLeft, showAddPage } from '../actions'
import Navbar from '../components/Navbar'

const mapStateToProps = state => {
    // console.log(state);
    return ({
    dropdownActive: state.navbar.active,
    dropleftActive: state.navbar.dropleftActive
})
}

const mapDispatchToProps = dispatch => ({
    onMenuShow: () => dispatch(showMenu()),
    onAddClick: () => dispatch(showHideDrp()),
    onDropleftClick: () => dispatch(showHideDrpLeft()),
    onAddPageShow: () => dispatch(showAddPage())
})

export default connect(mapStateToProps, mapDispatchToProps)(Navbar);