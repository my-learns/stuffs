import React from 'react'
import { Button } from '../../main/components/components'
import './index.scss'

export class DropLeft extends React.Component {
    render() {
        return (
            <div className="drp-left">
                <div className={"drp-left-btn" + (this.props.light ? " drp-left-btn-light" : "") + (this.props.active ? " drp-left-btn-active" : "")}>
                    <Button.Circle light={true} onClick={this.props.onClick}>
                        <i className={this.props.icon}></i>
                    </Button.Circle>
                </div>
                <div className={"drp-left-menu " + (this.props.active ? "drp-left-menu-show" : "drp-left-menu-hide")}>
                    {this.props.children}
                </div>

            </div>
        )
    }
}

DropLeft.Item = function(props) {
    return (
        <div className={"drp-left-item drp-left-item-"+props.pos + (props.active ? "-show" : "-hide")} title={props.title}>
            <div className={"drp-left-btn" + (props.light ? " drp-left-btn-light" : "")}>
                <Button.Circle light={true} onClick={props.onClick ? props.onClick : () => {}} path={props.path}>
                    <i className={props.icon}></i>
                </Button.Circle>
            </div>
        </div>
    )
}