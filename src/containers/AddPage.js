import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import "../style.scss"
import {ButtonXL} from '../components'
import { Link, Switch, Route } from 'react-router-dom'
import Cover from './Cover'
import { Card } from '../main/cards/CardAlt'
import { showAlert, addPageRemoveField, addPageEditField, FIELD_TYPE } from '../actions'
// import { Field } from '../main/components/components'


// import Field from '../components/Field'
import EField from '../components/EField'

const AddPage = ({data, onAlert, handleFieldSave, onFieldRemove}) => (
    <div className="page container pt-3">
        <Switch>
            <Route path="/add/ideas">
                
                <div className="row">
                    <div className="col-md-4">
                        <div className="box">
                            left
                        </div>

                        <div className="box">
                            left
                        </div>
                    </div>

                    <div className="col-md-8">

                        <Card blue={true}>
                            <Card.Header>
                                <Card.Header.Icon icon={data.icon} />
                                <Card.Header.Title>{data.title}</Card.Header.Title>
                            </Card.Header>
                            <Card.Body>
                                {data.fields.map((field) => {
                                    switch (field.type) {
                                        case FIELD_TYPE.TEXT:
                                            return <EField text={field.text} key={field.id} id={field.id} onRemove={() => onFieldRemove(field.id)} handleBlur={handleFieldSave} />;
                                        case FIELD_TYPE.LINK:
                                            return <EField.Link href={field.text} key={field.id} id={field.id} onRemove={() => onFieldRemove(field.id)} />;
                                        case FIELD_TYPE.PASSWORD:
                                            return <div></div>;
                                        default:
                                            return <div></div>;
                                    }
                                })}
                            </Card.Body>
                        </Card>

                        <div className="box">
                            <div className="box-header">Title</div>
                            <div className="box-body">
                                body<br />
                                body<br />
                                body<br />
                                body<br />
                            </div>
                        </div>
                    </div>
                </div>

               
            </Route>

            <Route path="/add/links">
                <Cover />
                <div className="cover-content d-flex justify-content-center align-items-center">
                    <div className="bg-light text-dark w-50 h-50 rounded rounded-xl p-3 pt-2">
                        Epta
                    </div>
                </div>
            </Route>

            <Route path="/add/ideas">
                <div>Ideas</div>
            </Route>

            <Route path="/add">
                <div className="d-flex">
                    <Link to="/add/credentials" className="flex-fill">
                        <ButtonXL icon="fa fa-lock">Credentials</ButtonXL>
                    </Link>
                    <Link to="/add/links" className="flex-fill">
                        <ButtonXL icon="fa fa-link">Links</ButtonXL>
                    </Link>
                    <Link to="/add/ideas" className="flex-fill">
                        <ButtonXL icon="fa fa-lightbulb">Ideas</ButtonXL>
                    </Link>
                </div>
            </Route>
        </Switch>
    </div>
)

AddPage.propTypes = {
    data: PropTypes.object
}

const mapStateToProps = state => ({
    ...state.addpage
})

const mapDispatchToProps = dispatch => ({
    onAlert: alert => dispatch(showAlert(alert)),
    onFieldRemove: id => dispatch(addPageRemoveField(id)),
    handleFieldSave: field => dispatch(addPageEditField(field)),
})

export default connect(mapStateToProps, mapDispatchToProps)(AddPage)