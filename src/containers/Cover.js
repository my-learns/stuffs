import React from 'react'
import PropTypes from 'prop-types'
// import { connect } from 'react-redux'
// import { hideCover } from '../actions'

const Cover = ({active, children, onClick}) => (
    <div className={"cover " + (active ? "cover-active" : "cover-inactive")} onClick={onClick}>
        <div onClick={e => {e.stopPropagation()}} class="cover-content">
            {children}
        </div>
    </div>
)

Cover.propTypes = {
    onClick: PropTypes.func,
    children: PropTypes.node
}

export default Cover;

// const mapStateToProps = state => ({
//     ...state.cover
// })

// const mapDispatchToProps = dispatch => ({
//     onClick: () => dispatch(hideCover())
// })

// export default connect(mapStateToProps, mapDispatchToProps)(Cover);